import { createApp } from 'vue';
import { path, fs } from '@tauri-apps/api';

import App from './App.vue';
import store from './store.js';
import i18n from './mixins/i18n.js';

const app = createApp(App);

app.use(store);

app.mixin(i18n);

(async () => {
    try {
        await fs.createDir(await path.appDir());
    }
    catch {}
    app.mount('#app');
})();