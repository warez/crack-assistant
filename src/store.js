import { createStore } from 'vuex';
import { fs, path } from '@tauri-apps/api';

const
    defaultSettings = {
        language: ['en', 'fr'].includes(window.navigator.language) ? window.navigator.language : 'en',
        workingDirectoryOverride: null
    },
    getSettingsFilePath = async () => await path.appDir() + 'settings.json',
    store = createStore({
        state: () => ({
            settings: {}
        }),
        mutations: {
            setSettings: async (state, data = {}) => {
                state.settings = {
                    ...defaultSettings,
                    ...data
                };
            },
            patchSettings: async (state, data) => {
                state.settings = {
                    ...defaultSettings,
                    ...state.settings,
                    ...data
                };
            }
        },
        actions: {
            readSettings: async ({ commit }) => {
                try {
                    commit('setSettings', JSON.parse(await fs.readTextFile(await getSettingsFilePath())));
                }
                catch {
                    commit('setSettings');
                }
            },
            writeSettings: async ({ state }) => {
                await fs.writeFile({
                    contents: JSON.stringify(state.settings, null, 4),
                    path: await getSettingsFilePath()
                });
            }
        }
    });

export default store;