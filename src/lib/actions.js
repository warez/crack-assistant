import { fs, shell, dialog, path } from '@tauri-apps/api';

import store from '../store.js';

const
    execute = async command => {
        const
            [program, ...args] = command.split(' '),
            { stdout, stderr } = await new shell.Command(program, args).execute();
        if(stderr)
            throw new Error(stderr);
        else
            return stdout;
    },
    executeAsAdmin = command => {
        const [program, ...args] = command.split(' ');
        return execute(`powershell Start-Process -Wait -WindowStyle hidden ${program} -Verb runAs -ArgumentList "${args.join(' ')}"`);
    },
    /**
     * @param {String} sourceFilePath
     * @param {String} targetFilePath
     * @returns {Promise<void>}
     */
    backupAndReplaceFile = async ({ sourceFilePath, targetFilePath }) => {
        const absoluteSourceFilePath = await execute(`powershell [IO.Path]::GetFullPath([io.path]::Combine("${store.state.settings.workingDirectoryOverride || '$(Get-Location)'}", "${sourceFilePath}"))`);
        await executeAsAdmin(`cmd /c move ""${targetFilePath}"" ""${targetFilePath}.bak""`);
        await executeAsAdmin(`cmd /c copy ""${absoluteSourceFilePath}"" ""${targetFilePath}""`);
    },
    /**
     * @param {String[]} hostEntries
     * @returns {Promise<void>}
     */
    addHostEntries = async ({ hostEntries }) => {
        const
            hostsFilePath = 'C:\\Windows\\System32\\drivers\\etc\\hosts',
            currentHostEntries = (await fs.readTextFile(hostsFilePath))
                .split('\n')
                .filter(line => !line.startsWith('#')),
            newHostEntries = hostEntries.filter(entry => !currentHostEntries.includes(entry)),
            newLine = '`r`n';
        return await executeAsAdmin(`powershell Add-Content -Path ${hostsFilePath} -Value '${newLine}${newHostEntries.join(newLine)}'`);
    },
    /**
     * @param {String} path
     * @returns {Promise<void>}
     */
    addFirewallBlockRule = async ({ path }) => {
        for(const direction of ['in', 'out'])
            await executeAsAdmin(`netsh advfirewall firewall add rule name=\`"[CrackAssistant] Block ${path}\`" dir=${direction} action=block profile=any program=\`"${path}\`"`);
    },
    /**
     * @param {String} path
     * @param {String} key
     * @param {String} value
     * @param {('BINARY'|'DWORD'|'QWORD'|'SZ')} type
     * @returns {Promise}
     */
    createOrUpdateRegistryEntry = ({ path, key, value, type }) =>
        executeAsAdmin(`reg add ${path} -v ${key} -t REG_${type} -d ${value} -f`),
    openUrl = async url => await shell.open(url),
    openDirectoryDialog = async () => await dialog.open({
        defaultPath: await path.currentDir(),
        directory: true
    });

export {
    backupAndReplaceFile,
    addHostEntries,
    addFirewallBlockRule,
    createOrUpdateRegistryEntry,
    openUrl,
    openDirectoryDialog
};