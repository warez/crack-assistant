import { path, fs } from '@tauri-apps/api';

import store from '../store.js';

const
    getConfigFilePath = async () => {
        const { workingDirectoryOverride } = store.state.settings;
        return (workingDirectoryOverride ? `${workingDirectoryOverride}\\` : await path.currentDir()) + 'crack-assistant.json';
    },
    getConfig = async () => {
        try {
            return JSON.parse(await fs.readTextFile(await getConfigFilePath()));
        }
        catch {
            return {};
        }
    },
    setConfig = async data => await fs.writeFile({
        contents: JSON.stringify(data, null, 4),
        path: await getConfigFilePath()
    }),
    patchConfig = async data => {
        const config = await getConfig();
        Object.assign(config, data);
        await setConfig(config);
    };

export {
    getConfig,
    patchConfig
};