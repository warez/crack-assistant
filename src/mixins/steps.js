import { nanoid } from 'nanoid';

import { getConfig, patchConfig } from '../lib/config.js';

export default {
    data: () => ({
        steps: []
    }),
    methods: {
        updateSteps: function(){
            return patchConfig({ steps: this.steps });
        }
    },
    mounted: async function(){
        const { steps = [] } = await getConfig();
        this.steps.push(...steps);
    },
    watch: {
        steps: {
            deep: true,
            handler: function(){
                this.steps.forEach(async step => {
                    if(!step.id)
                        step.id = nanoid();
                    await this.updateSteps();
                });
            }
        }
    }
};