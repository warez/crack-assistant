import i18n from '../data/i18n.json'
export default {
    computed: {
        i18n: function(){
            return i18n[this.$store.state.settings.language] || {};
        }
    }
};