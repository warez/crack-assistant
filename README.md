# Crack Assistant

Crack Assistant automates usual software crack setup steps :
- Backing up & replacing files
- Adding host entries
- Adding firewall block rules
- Adding or updating registry entries

## Usage

### `Setup` tab

Create a crack setup configuration.

![](https://i.goopics.net/q5xumi.png)

This will generate a `crack-assistant.json` file in your working directory.

### `Run` tab

Run the current crack setup configuration

![](https://i.goopics.net/t8bxuo.png)

Launch one, more or all configured cracking steps.

[UAC](https://en.wikipedia.org/wiki/User_Account_Control) will be triggered one or two times at each step.

### `Settings` tab

Change your language, override the working directory for testing purposes, check your current version, visit the open source repository.

![](https://i.goopics.net/wpilwx.png)